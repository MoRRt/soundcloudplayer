﻿using System;
using System.Diagnostics;
using Prism.Unity;
using SoundCloudPlayer.Services;
using SoundCloudPlayer.Views;
using Microsoft.Practices.Unity;
using Plugin.MediaManager.Forms;
using SoundCloudPlayer.ViewModels.Mappings;
using Xamarin.Forms;

namespace SoundCloudPlayer
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            try
            {

                InitializeComponent();
                AutoMapperConfiguration.Configure();
                NavigationService.NavigateAsync("NavigationPage/GeneralTabbedPage/SearchUsersPage/");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<NavigationPage>();
            Container.RegisterTypeForNavigation<TrackListPage>();
            //Container.RegisterInstance(typeof(ISoundCloudService), new SoundCloudService(), new ContainerControlledLifetimeManager());
            Container.RegisterType<ISoundCloudService, SoundCloudService>(new ContainerControlledLifetimeManager());
            Container.RegisterTypeForNavigation<MediaPlayerPage>();
            Container.RegisterTypeForNavigation<SearchUsersPage>();
            Container.RegisterTypeForNavigation<ResultsMapPage>();
            Container.RegisterTypeForNavigation<GeneralTabbedPage>();
        }
    }
}
