﻿using System.Globalization;
using System.Linq;
using System.Windows.Input;
using Microsoft.Practices.Unity.Utility;
using Xamarin.Forms;

namespace SoundCloudPlayer.Command
{
    public class InvokeCommandAction : TriggerAction<Element>
    {
        #region Command
        private ICommand command = null;
        public Binding Command { get; set; }

        #endregion //Command

        #region CommandParameter
        private object commandParameter = null;
        public Binding CommandParameter { get; set; }
        #endregion //CommandParameter

        protected override void Invoke(Element sender)
        {
            if (this.Command == null || this.Command.Path == null || sender.BindingContext == null)
            {
                return;
            }


            var bindingContext = sender.BindingContext;
            if (string.IsNullOrWhiteSpace(this.Command.Path))
            {
                this.command = bindingContext as ICommand;
            }
            else
            {
                var value = (from p in bindingContext.GetType().GetPropertiesHierarchical()
                    where p.CanRead && this.Command.Path.Equals(p.Name)
                    select p.GetValue(bindingContext)).FirstOrDefault();
                if (this.Command.Converter != null)
                {
                    value = this.Command.Converter.Convert(
                        value,
                        typeof(ICommand),
                        this.Command.ConverterParameter,
                        CultureInfo.CurrentCulture);
                }
                this.command = value as ICommand;
            }
            if (string.IsNullOrWhiteSpace(this.CommandParameter.Path))
            {
                this.commandParameter = bindingContext;
            }
            else
            {
                var value = (from p in bindingContext.GetType().GetPropertiesHierarchical()
                    where p.CanRead && this.CommandParameter.Path.Equals(p.Name)
                    select p.GetValue(bindingContext)).FirstOrDefault();
                if (this.CommandParameter.Converter != null)
                {
                    value = this.CommandParameter.Converter.Convert(
                        value,
                        typeof(object),
                        this.CommandParameter.ConverterParameter,
                        CultureInfo.CurrentCulture);
                }
                this.commandParameter = value;
            }


            if (this.command == null || !this.command.CanExecute(this.commandParameter))
            {
                return;
            }

            this.command.Execute(this.commandParameter);
        }
    }
}