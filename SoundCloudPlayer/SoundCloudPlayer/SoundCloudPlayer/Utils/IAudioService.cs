﻿namespace SoundCloudPlayer.Utils
{
    public interface IAudioService
    {
        bool PlayPause(string url);
        bool Stop(bool val);
    }
}