﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Acr.UserDialogs;
using Refit;
using SoundCloudPlayer.Models.DtoEntities;

namespace SoundCloudPlayer.Utils
{
    public class ApiErrorHandler
    {
        public static void HandleError<TException, TResponse>(TException exception, out TResponse response)
        {
            response = default(TResponse);
            if (exception is ApiException)
            {
                var apiEx = exception as ApiException;
                UserDialogs.Instance.Toast($"Server error. \n Content: {apiEx.Content} \n Status code: {apiEx.StatusCode}");
                //if (typeof(TResponse) != typeof(IEnumerable<TrackDto>))
                //    response = apiEx.GetContentAs<TResponse>();
            }
            else
            {
                var ex = exception as Exception;
                //Debug.WriteLine(ex?.Message);
                UserDialogs.Instance.Toast($"Application error. \n {ex.Message}");
            }
        }
    }
}