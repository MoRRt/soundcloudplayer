﻿using System.Collections.Generic;
using Prism.Events;
using SoundCloudPlayer.Models.DtoEntities;

namespace SoundCloudPlayer.Events
{
    public class PassedEvent : PubSubEvent<IEnumerable<FacetCollection>>
    {
        
    }
}