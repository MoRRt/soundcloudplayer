﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using Prism.Events;
using Prism.Navigation;
using Prism.Services;
using SoundCloudPlayer.Events;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;
using SoundCloudPlayer.Services;
using Xamarin.Forms.GoogleMaps;

namespace SoundCloudPlayer.ViewModels
{
    public class ResultsMapPageViewModel : BaseViewModel
    {
        private const int LimitItems = 30;
        private readonly ISoundCloudService _soundCloudService;

        private DelegateCommand<Map> _initialMapCommand;
        public ICommand InitialMapCommand => _initialMapCommand ?? (_initialMapCommand =
                                                   new DelegateCommand<Map>(OnInitialMapCommandExecuted));

        private DelegateCommand _centerMapCommand;
        public DelegateCommand CenterMapCommand => _centerMapCommand ?? (_centerMapCommand =
                                                 new DelegateCommand(OnCenterMapCommandExecuted));

        private void OnInitialMapCommandExecuted(Map map)
        {
            MapControl = map;
        }

        private void OnCenterMapCommandExecuted()
        {
            if (MapControl?.VisibleRegion == null)
            {MapControl?.MoveToRegion(new MapSpan(new Position(0, 0), 0, 0).WithZoom(0));}
        }

        private Map _mapControl;
        public Map MapControl
        {
            get { return _mapControl; }
            set { SetProperty(ref _mapControl, value); }
        }

        private User _user;
        public User User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private ObservableCollection<User> _followers;
        public ObservableCollection<User> Followers
        {
            get { return _followers; }
            set { SetProperty(ref _followers, value); }
        }

        private List<Pin> _listPin;
        public List<Pin> ListPin
        {
            get { return _listPin; }
            set { SetProperty(ref _listPin, value); }
        }

        private Position _pos;
        public Position Pos
        {
            get { return _pos; }
            set { SetProperty(ref _pos, value); }
        }

        public ResultsMapPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ISoundCloudService soundCloudService, IEventAggregator eventAggregator)
            : base(navigationService)
        {
            _soundCloudService = soundCloudService;
            Followers = new ObservableCollection<User>();
            eventAggregator.GetEvent<PassedEvent>().Subscribe(Handler);
        }

        private async void Handler(IEnumerable<FacetCollection> facetCollections)
        {
            try
            {
                MapControl.Pins.Clear();
                Geocoder geocoder = new Geocoder();
                FacetCollection facet = facetCollections.FirstOrDefault();
                if (facet != null)
                {
                    foreach (var place in facet.Facets)
                    {
                        string address = place.Value;
                        if (!string.IsNullOrEmpty(address))
                        {
                            IEnumerable<Position> positions = await geocoder.GetPositionsForAddressAsync(address);
                            Position point = positions.FirstOrDefault();
                            if (point != default(Position))
                                MapControl.Pins.Add(
                                    new Pin
                                    {
                                        Position = point,
                                        Address = Convert.ToString(place.Count),
                                        Label = address.Substring(0,1).ToUpper() + address.Remove(0,1)
                                    });
                        }
                    }
                }
                MapControl.MoveToRegion(MapControl.VisibleRegion.WithZoom(0));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
      

        private async Task<IEnumerable<User>> GetFollowers(int userId, int offset)
        {
            UserCollectionDto responseFollowers = await _soundCloudService.GetUserFollowers(userId, offset);
            IEnumerable<User> followersList =
                Mapper.Map<IEnumerable<UserDto>, IEnumerable<User>>(responseFollowers.UserList);
            return followersList;
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("user"))
            {
                User = (User)parameters["user"];
            }
        }
    }
}
