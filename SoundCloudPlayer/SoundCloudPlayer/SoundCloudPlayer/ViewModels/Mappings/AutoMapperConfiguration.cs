﻿using System;
using System.Diagnostics;
using AutoMapper;

namespace SoundCloudPlayer.ViewModels.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                try
                {
                    x.AddProfile<DtoToViewModelMappingProfile>();
                    x.AddProfile<ViewModelToDtoMappingProfile>();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            });
        }
    }
}