﻿using AutoMapper;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;

namespace SoundCloudPlayer.ViewModels.Mappings
{
    public class ViewModelToDtoMappingProfile : Profile
    {
        public ViewModelToDtoMappingProfile()
        {
            CreateMap<Track, TrackDto>();
        }
    }
}