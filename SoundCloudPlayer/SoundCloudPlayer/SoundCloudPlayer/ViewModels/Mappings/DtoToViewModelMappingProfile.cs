﻿using System;
using AutoMapper;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;
using SoundCloudPlayer.Services;

namespace SoundCloudPlayer.ViewModels.Mappings
{
    public class DtoToViewModelMappingProfile : Profile
    {
        public DtoToViewModelMappingProfile()
        {
            CreateMap<TrackDto, Track>()
                .ForMember(vm => vm.StreamUrl,
                    map => map.MapFrom(s => s.StreamUrl + $"?client_id={SoundCloudService.ClientId}"))
                .ForMember(vm => vm.Duration,
                    map => map.MapFrom(s => TimeSpan.FromSeconds(s.Duration).ToString(@"mm\:ss")))
                .ForMember(vm => vm.User, map => map.MapFrom(s => s.User));
            CreateMap<UserDto, User>();
        }
    }
}