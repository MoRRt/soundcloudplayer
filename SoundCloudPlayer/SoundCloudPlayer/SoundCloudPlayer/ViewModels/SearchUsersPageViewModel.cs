﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using Plugin.MediaManager.Abstractions.Implementations;
using Prism.Events;
using Prism.Navigation;
using Prism.Services;
using SoundCloudPlayer.CustomControls;
using SoundCloudPlayer.Events;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;
using SoundCloudPlayer.Services;
using Xamarin.Forms;

namespace SoundCloudPlayer.ViewModels
{
	public class SearchUsersPageViewModel : BaseViewModel
	{
        private const int LimitItems = 30;
	    private readonly ISoundCloudService _soundCloudService;
	    private readonly IEventAggregator _eventAggregator;

	    public SearchUsersPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ISoundCloudService soundCloudService, IEventAggregator eventAggregator)
	        : base(navigationService)
	    {
	        _eventAggregator = eventAggregator;
            _soundCloudService = soundCloudService;
            Users = new ObservableCollection<User>();
	        _eventAggregator.GetEvent<PassedEvent>().Publish(null);
        }
        private DelegateCommand<User> _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand =
                                                 new DelegateCommand<User>(OnItemTappedCommandExecuted));

        private DelegateCommand _searchPressCommand;
	    public DelegateCommand SearchPressCommand => _searchPressCommand ?? (_searchPressCommand =
	                                                 new DelegateCommand(OnSearchPressCommandExecuted));

	    private DelegateCommand _loadingMoreCommand;
	    public DelegateCommand LoadingMoreCommand => _loadingMoreCommand ?? (_loadingMoreCommand =
	                                                new DelegateCommand(OnLoadingMoreCommandExecuted,() => !IsLoading).ObservesProperty(() => IsLoading));

        private DelegateCommand _refreshCommand;
	    public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
	                                                 new DelegateCommand(OnRefreshCommandExecuted));

	    private bool _isLoading;
	    public bool IsLoading
        {
	        get { return _isLoading; }
	        set { SetProperty(ref _isLoading, value); }
	    }

        private object _bindingContext;
	    public object BindingContext
        {
	        get { return _bindingContext; }
	        set { SetProperty(ref _bindingContext, value); }
	    }


        private string _query;
	    public string Query
	    {
	        get { return _query; }
	        set { SetProperty(ref _query, value); }
	    }

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set { SetProperty(ref _users, value); }
        }

        private bool _isRefreshing;
	    public bool IsRefreshing
	    {
	        get { return _isRefreshing; }
	        set { SetProperty(ref _isRefreshing, value); }
	    }

        private async void OnItemTappedCommandExecuted(User user)
	    {
	        var parameters = new NavigationParameters { { "user", user } };
	       await _navigationService.NavigateAsync("TrackListPage", parameters,false);
	    }


        private async void OnLoadingMoreCommandExecuted()
	    {
	        int offset = Users.Count;
            await SearchUsers(Query, offset);
        }


        private async void OnRefreshCommandExecuted()
	    {
	        IsRefreshing = true;
            Users?.Clear();
	        int offset = Users.Count;
	        await SearchUsers(Query, offset);
            IsRefreshing = false;
	       
        }
	   

        private async void OnSearchPressCommandExecuted()
	    {
            IsBusy = true;
	        Users?.Clear();
	        int offset = Users.Count;
            await SearchUsers(Query, offset);
	        IsBusy = false;
        }

	    private async Task SearchUsers(string query, int offset)
	    {
	        try
	        {
	            if (string.IsNullOrEmpty(Query)) return;
	            IsLoading = true;
	            UserCollectionDto response =
	                await _soundCloudService.GetSearchUsers(query, offset, LimitItems); //user_id=3207,300731085,141715876,123327208
	            IEnumerable<User> users = Mapper.Map<IEnumerable<UserDto>, IEnumerable<User>>(response.UserList);
	            _eventAggregator.GetEvent<PassedEvent>().Publish(response.Facets);
                Users.AddRange(users);
	            IsLoading = false;
	        }
	        catch (Exception e)
	        {
                Debug.WriteLine(e);
	        }
	    }
	}
}
