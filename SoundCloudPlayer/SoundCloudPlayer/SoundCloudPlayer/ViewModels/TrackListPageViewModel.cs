﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using Plugin.MediaManager.Abstractions.Implementations;
using Prism.Services;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;
using SoundCloudPlayer.Services;
using SoundCloudPlayer.Utils;
using Xamarin.Forms;

namespace SoundCloudPlayer.ViewModels
{
    public class TrackListPageViewModel : BaseViewModel
    {
        private const int LimitItems = 30;
        private User _user;
        public User User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }
        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetProperty(ref _isRefreshing, value);
        }

        private DelegateCommand<Track> _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand =
                                                     new DelegateCommand<Track>(OnItemTappedCommandExecuted));

        private DelegateCommand _refreshCommand;
        public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
                                                 new DelegateCommand(OnRefreshCommandExecuted));


        private DelegateCommand _loadingMoreCommand;
        public DelegateCommand LoadingMoreCommand => _loadingMoreCommand ?? (_loadingMoreCommand =
                                                         new DelegateCommand(OnLoadingMoreCommandExecuted, () => !IsLoading).ObservesProperty(() => IsLoading));

        private async void OnLoadingMoreCommandExecuted()
        {
            int offset = Tracks.Count;
            await LoadTracks(offset);
        }


        private void OnItemTappedCommandExecuted(Track track)
        {
            var parameters = new NavigationParameters {{"track", track}};
            _navigationService.NavigateAsync("MediaPlayerPage", parameters);

        }

        private async void OnRefreshCommandExecuted()
        {
            IsRefreshing = true;
            try
            {
                Tracks?.Clear();
                int offset = Tracks.Count;
                await LoadTracks(offset);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            IsRefreshing = false;
        }


        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }

        private ISoundCloudService _soundCloudService;
        public ISoundCloudService SoundCloudService
        {
            get => _soundCloudService;
            set => SetProperty(ref _soundCloudService, value);
        }

        public TrackListPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ISoundCloudService soundCloudService)
            : base(navigationService)
        {
            SoundCloudService = soundCloudService;
            Tracks = new ObservableCollection<Track>();
        }

        private ObservableCollection<Track> _tracks;
        public ObservableCollection<Track> Tracks
        {
            get { return _tracks; }
            set { SetProperty(ref _tracks, value); }
        }

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            IsBusy = true;
            int offset = Tracks.Count;
            await LoadTracks(offset);
            IsBusy = false;

        }


        private async Task LoadTracks(int offset)
        {
            try
            {
                IsLoading = true;
                IEnumerable<TrackDto> tracksDto =
                    await SoundCloudService.GetTracksByUser(Convert.ToInt32(User.Id), offset, LimitItems); //user_id=3207,300731085,141715876,123327208
                IEnumerable<Track> tracks = Mapper.Map<IEnumerable<TrackDto>, IEnumerable<Track>>(tracksDto);
                Tracks.AddRange(tracks);
                IsLoading = false;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("user"))
            {
                User = (User)parameters["user"];
            }
        }

       
    }
}
