﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Plugin.MediaManager;
using Plugin.MediaManager.Abstractions;
using Prism.Navigation;
using Prism.Services;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;
using SoundCloudPlayer.Services;
using SoundCloudPlayer.Utils;
using Xamarin.Forms;

namespace SoundCloudPlayer.ViewModels
{
    public class MediaPlayerPageViewModel : BaseViewModel
    {
        private IMediaManager MediaController => CrossMediaManager.Current;
        private IAudioService _audioService;
        public IAudioService AudioService
        {
            get => _audioService;
            set => SetProperty(ref _audioService, value);
        }
        public MediaPlayerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService)
        {
        }


        private DelegateCommand _stopAudioCommand;
        public DelegateCommand StopAudioCommand => _stopAudioCommand ?? (_stopAudioCommand =
                                                       new DelegateCommand(OnStopAudioCommandExecuted));
        private DelegateCommand _playAudioCommand;
        public DelegateCommand PlayAudioCommand => _playAudioCommand ?? (_playAudioCommand =
                                                       new DelegateCommand(OnPlayAudioCommandExecuted));

        private DelegateCommand _pauseAudioCommand;
        public DelegateCommand PauseAudioCommand => _pauseAudioCommand ?? (_pauseAudioCommand =
                                                       new DelegateCommand(OnPauseAudioCommandExecuted));

        private Track _track;

        public Track Track
        {
            get => _track;
            set => SetProperty(ref _track, value);
        }
     

        private double _progress;
        public double Progress
        {
            get => _progress;
            set => SetProperty(ref _progress, value);
        }

        private string _duration;

        public string Duration
        {
            get => _duration;
            set => SetProperty(ref _duration, value);
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("track"))
            {
                //TrackDto track = (TrackDto) parameters["track"];
                //Track = Mapper.Map<TrackDto, Track>(track);
                Track = (Track)parameters["track"];
            }

            MediaController.PlayingChanged += (sender, e) =>
            {
                Progress = e.Progress;
                Duration = TimeSpan.FromSeconds(e.Duration.TotalSeconds).ToString(@"mm\:ss");

            };
        }
        private async void OnPlayAudioCommandExecuted()
        {
            IsBusy = true;
            await MediaController.Play(Track.StreamUrl);
            IsBusy = false;
        }

        private async void OnStopAudioCommandExecuted()
        {
            IsBusy = true;
            await MediaController.Stop();
            IsBusy = false;
        }

        private async void OnPauseAudioCommandExecuted()
        {
            IsBusy = true;
            await MediaController.Pause();
            IsBusy = false;
        }
    }
}
