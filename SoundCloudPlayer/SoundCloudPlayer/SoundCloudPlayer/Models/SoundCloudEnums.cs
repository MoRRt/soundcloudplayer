﻿namespace SoundCloudPlayer.Models
{
    public class SoundCloudEnums
    {
        public enum State
        {
            Finished, Processing, Failed
        }
        public enum Kind
        {
            User, Track, App, Playlist
        }

        public enum Sharing
        {
            Public, Private
        }
    }
}