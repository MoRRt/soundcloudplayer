﻿using Prism.Mvvm;
using Prism.Navigation;
using SoundCloudPlayer.Models.DtoEntities;
using SoundCloudPlayer.ViewModels;

namespace SoundCloudPlayer.Models
{
    public class Track : BaseSoundCloudModel
    {
        public string Duration { get; set; }
        public string Title { get; set; }
        public string Uri { get; set; }
        public string StreamUrl { get; set; }
        public User User { get; set; }

    }
}