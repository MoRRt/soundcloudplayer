﻿using Refit;

namespace SoundCloudPlayer.Models
{
    public struct SearchFilter
    {
        [AliasAs("q")]
        public string Query { get; set; }
    }
}