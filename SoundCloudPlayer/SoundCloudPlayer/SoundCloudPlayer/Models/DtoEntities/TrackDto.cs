﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace SoundCloudPlayer.Models.DtoEntities
{
    public class TrackDto : BaseSoundCloudModel
    {
        [JsonProperty("user_id")]
        public int UserId { get; set; }

        [JsonProperty("state")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SoundCloudEnums.State State { get; set; }

        [JsonProperty("sharing")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SoundCloudEnums.Sharing Sharing { get; set; }
        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }

        [JsonProperty("stream_url")]
        public string StreamUrl { get; set; }
        [JsonProperty("user")]
        public UserDto User { get; set; }
    }
}