﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SoundCloudPlayer.Models.DtoEntities
{
    public class UserCollectionDto
    {
        [JsonProperty("collection")]
        public IEnumerable<UserDto> UserList { get; set; }
        [JsonProperty("next_href")]
        public string NextPage { get; set; }
        [JsonProperty("total_results")]
        public int TotalResults { get; set; }
        [JsonProperty("facets")]
        public IEnumerable<FacetCollection> Facets { get; set; }

    }
}