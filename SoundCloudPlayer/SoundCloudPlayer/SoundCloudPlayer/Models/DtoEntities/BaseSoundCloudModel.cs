﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SoundCloudPlayer.Models.DtoEntities
{
    public class BaseSoundCloudModel 
    {
        public int? Id { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public SoundCloudEnums.Kind Kind { get; set; }
    }
}