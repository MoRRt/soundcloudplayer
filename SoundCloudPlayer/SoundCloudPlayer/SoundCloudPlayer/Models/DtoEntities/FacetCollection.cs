﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SoundCloudPlayer.Models.DtoEntities
{
    public class FacetCollection
    {
        [JsonProperty("facets")]
        public IEnumerable<Facet> Facets { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}