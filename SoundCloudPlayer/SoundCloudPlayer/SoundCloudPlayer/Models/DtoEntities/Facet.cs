﻿using Newtonsoft.Json;

namespace SoundCloudPlayer.Models.DtoEntities
{
    public class Facet
    {
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}