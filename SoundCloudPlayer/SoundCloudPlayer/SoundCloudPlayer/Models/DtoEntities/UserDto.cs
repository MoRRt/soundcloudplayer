﻿using Newtonsoft.Json;

namespace SoundCloudPlayer.Models.DtoEntities
{
    public class UserDto : BaseSoundCloudModel
    {
        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("followers_count")]
        public int FollowersCount { get; set; }
    }
}