﻿using SoundCloudPlayer.Models.DtoEntities;

namespace SoundCloudPlayer.Models
{
    public class User : BaseSoundCloudModel
    {
        public string UserName { get; set; }
        public string Uri { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public int FollowersCount { get; set; }
    }
}