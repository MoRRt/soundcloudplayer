﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;

namespace SoundCloudPlayer.Services
{
    public interface ISoundCloudService
    {
        Task<UserCollectionDto> GetSearchUsers(string query, int offset, int limit);
        Task<IEnumerable<TrackDto>> GetTracksByUser(int userId, int offset, int limit);
        Task<UserCollectionDto> GetUserFollowers(int userId, int pageSize);
    }
}