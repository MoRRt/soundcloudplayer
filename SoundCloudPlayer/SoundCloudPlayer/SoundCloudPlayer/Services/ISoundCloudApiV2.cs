﻿using System.Threading.Tasks;
using Refit;
using SoundCloudPlayer.Models.DtoEntities;

namespace SoundCloudPlayer.Services
{
    public interface ISoundCloudApiV2
    {
        [Get("/search/users?client_id=9ac2b17027e4af068adbb4f10330e1b3&q={query}&offset={offset}&limit={limit}&facet=place")]
        Task<UserCollectionDto> GetSearchUsers(string query, int offset, int limit);
    }
}