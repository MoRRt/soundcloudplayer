﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;

namespace SoundCloudPlayer.Services
{
    public interface ISoundCloudApi
    {
        [Get("/users/{userId}/tracks?client_id=9ac2b17027e4af068adbb4f10330e1b3&offset={offset}&limit={limit}")]
        Task<IEnumerable<TrackDto>> GetTracksByUser(int userId, int offset, int limit);
        //[Get("/users/?client_id=9ac2b17027e4af068adbb4f10330e1b3&q={query}&offset={offset}&limit={limit}&linked_partitioning=1")]
        //Task<UserCollectionDto> GetSearchUsers(string query, int offset, int limit);
        [Get("/users/{userId}/followers?client_id=9ac2b17027e4af068adbb4f10330e1b3&page_size={pageSize}")]
        Task<UserCollectionDto> GetUserFollowers(int userId, int pageSize);
    }
}