﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Plugin.Connectivity;
using Polly;
using Refit;
using SoundCloudPlayer.Models;
using SoundCloudPlayer.Models.DtoEntities;
using SoundCloudPlayer.Utils;

namespace SoundCloudPlayer.Services
{
    public class SoundCloudService : ISoundCloudService
    {
        public static string ClientId { get; } = "9ac2b17027e4af068adbb4f10330e1b3";

        private const int RequestRetryCount = 5;
        public ISoundCloudApi Api { get; set; }
        public ISoundCloudApiV2 ApiV2 { get; set; }
        private string _baseUrl = "http://api.soundcloud.com";
        private string _baseUrlV2 = "http://api-v2.soundcloud.com";

        public SoundCloudService()
        {
            HttpClient httpClient = new HttpClient
            {
                BaseAddress = new Uri(_baseUrl)
            };
            HttpClient httpClientV2 = new HttpClient
            {
                BaseAddress = new Uri(_baseUrlV2)
            };
            Api = RestService.For<ISoundCloudApi>(httpClient);
            ApiV2 = RestService.For<ISoundCloudApiV2>(httpClientV2);
        }

        public async Task<UserCollectionDto> GetSearchUsers(string query , int offset, int limit)
        {
            Task<UserCollectionDto> requestTask = ApiV2.GetSearchUsers(query, offset, limit);
            var response = await GetResponse(requestTask);
            return response;
        }

        public async Task<IEnumerable<TrackDto>> GetTracksByUser(int userId, int offset, int limit)
        {
            Task<IEnumerable<TrackDto>> requestTask = Api.GetTracksByUser(userId, offset, limit);
            var response = await GetResponse(requestTask);
            return response;
        }

        public async Task<UserCollectionDto> GetUserFollowers(int userId, int pageSize)
        {
            Task<UserCollectionDto> requestTask = Api.GetUserFollowers(userId, pageSize);
            var response = await GetResponse(requestTask);
            return response;
        }

        private async Task<T> GetResponse<T>(Task<T> task)
        {
            T response = default(T);
            if (CrossConnectivity.Current.IsConnected)
            {
                try
                {
                    response = await Policy
                        .Handle<Exception>()
                        .RetryAsync(retryCount: RequestRetryCount)
                        .ExecuteAsync(async () => await task);
                }
                catch (Exception exception)
                {
                    ApiErrorHandler.HandleError<Exception, T>(exception, out response);
                }
            }

            return response;
        }
    }
}