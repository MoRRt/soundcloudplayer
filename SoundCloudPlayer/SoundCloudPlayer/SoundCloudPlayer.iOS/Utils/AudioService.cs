﻿using AVFoundation;
using Foundation;
using SoundCloudPlayer.iOS.Utils;
using Xamarin.Forms;
using SoundCloudPlayer.Utils;

[assembly: Dependency(typeof(AudioService))]
namespace SoundCloudPlayer.iOS.Utils
{
    public class AudioService : IAudioService
    {
        int clicks = 0;

        public AudioService() { }
        AVPlayer player;
        public bool PlayPause(string url)
        {
            if (clicks == 0)
            {
               
                //this.player = new AVAudioPlayer(NSUrl.FromString(url), "mp3", out err);
                this.player = new AVPlayer();
                this.player = AVPlayer.FromUrl(NSUrl.FromString(url));
                this.player.Play();
                clicks++;
            }
            else if (clicks % 2 != 0)
            {
                this.player.Pause();
                clicks++;

            }
            else
            {
                this.player.Play();
                clicks++;
            }
            return true;

        }

        public bool Stop(bool val)
        {
            if (player != null)
            {
                player.Dispose();
                clicks = 0;
            }
            return true;
        }
    }
}
