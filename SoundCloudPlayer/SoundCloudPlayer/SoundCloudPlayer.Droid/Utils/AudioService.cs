﻿using Android.Media;
using SoundCloudPlayer.Droid.Utils;
using SoundCloudPlayer.Utils;
using Xamarin.Forms;


[assembly: Dependency(typeof(AudioService))]
namespace SoundCloudPlayer.Droid.Utils
{
    public class AudioService : IAudioService
    {
        int clicks = 0;
        MediaPlayer player;

        public AudioService()
        {
        }

        public bool PlayPause(string url)
        {
            if (clicks == 0)
            {
                this.player = new MediaPlayer();
                this.player.SetDataSource(url);
                this.player.SetAudioStreamType(Stream.Music);
                this.player.PrepareAsync();
                this.player.Looping = true;
                this.player.SetVolume(10,10);
                this.player.Start();
                clicks++;
            }
            else if (clicks % 2 != 0)
            {
                this.player.Pause();
                clicks++;

            }
            else
            {
                this.player.Start();
                clicks++;
            }


            return true;
        }

        public bool Stop(bool val)
        {
            this.player.Stop();
            clicks = 0;
            return true;
        }
    }
}